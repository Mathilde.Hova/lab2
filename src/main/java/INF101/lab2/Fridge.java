package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {
    int max_size = 20;
    ArrayList<FridgeItem> fridge = new ArrayList<>();
    
    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return fridge.size();
        
        // return 0;
    }
    @Override
    public boolean placeIn(FridgeItem item) {
        
        if (nItemsInFridge() < max_size) {
            fridge.add (item);
            return true;
        }
        else {

        }
        return false;
    }
    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge()>0) {
            fridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }
    @Override
    public void emptyFridge() {
        if (nItemsInFridge()>0) {
            fridge.clear();
        }
        
        
    }
    @Override
    public List<FridgeItem> removeExpiredFood() {
        
        ArrayList<FridgeItem> deleteItems = new ArrayList<>();
        for (FridgeItem i : fridge) {
            if (i.hasExpired()) {
                deleteItems.add(i);
                // fridge.remove(i);
            }
        }
        for (FridgeItem n : deleteItems) {
            fridge.remove(n);
        }
        return deleteItems;
    }
   
    
}
